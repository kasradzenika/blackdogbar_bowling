﻿using UnityEngine;

public class ARCameraStabilizer : MonoBehaviour {

    //stabilizes motion from source and copies to target
    public Transform sourceObject;
    public Transform targetObject;

    public float damping;
    
	void Update ()
    {
        targetObject.localPosition = Vector3.Lerp(targetObject.localPosition, sourceObject.localPosition, damping * Time.deltaTime);
        targetObject.localRotation = Quaternion.Lerp(targetObject.localRotation, sourceObject.localRotation, damping * Time.deltaTime);
    }
}
