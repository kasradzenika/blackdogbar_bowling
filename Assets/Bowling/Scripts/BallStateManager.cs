﻿using UnityEngine;

public class BallStateManager : MonoBehaviour
{
    private Vector3 initialPosition;
    private Rigidbody myBody;

    public static BallStateManager instance;

    void Awake()
    {
        instance = this;
        initialPosition = transform.position;
        myBody = GetComponent<Rigidbody>();
        myBody.isKinematic = true;
    }

    //ball fell over the lane
    private void OnCollisionEnter(Collision collision)
    {
        if(collision.transform.tag == "failTrigger")
        {
            GetComponent<Renderer>().enabled = false;
            LaneManager.instance.BallFellOverLane();
        }
    }

    public void RollTheBall(Vector3 withVelocity)
    {
        Vector3 velocity = Vector3.zero;

        myBody.position = Camera.main.transform.position + Camera.main.transform.forward;

        myBody.isKinematic = false;

        //transform velocity
        velocity += Camera.main.transform.forward * withVelocity.y;
        velocity += Camera.main.transform.right * withVelocity.x;

        myBody.velocity = velocity;
        Debug.Log("Pushing the ball with: " + velocity);
    }

    public void ResetBall()
    {
        transform.position = initialPosition;
        GetComponent<Renderer>().enabled = true;
        myBody.velocity = Vector3.zero;
        myBody.isKinematic = true;
    }
}
