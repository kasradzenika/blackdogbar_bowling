﻿using UnityEngine;
using UnityEngine.Events;

public class LaneManager : MonoBehaviour
{
    public SessionState currentState { private set; get; }
    //public DidFinishSessionEvent didFinishSessionEvent;
    public OnSessionChangeEvent onSessionChangeEvent;
    public float swipeVelocityMultiplier = 10f;
    public float autoResetTime = -1f;
    public Vector2 debugFirstSwipe;
    public int lastScore = 0;

    private bool ballDidFellOver = false;

    public static LaneManager instance;

    void Awake ()
    {
        instance = this;
        currentState = SessionState.WaitingForSwipe;

        if (debugFirstSwipe != Vector2.zero)
            RollTheBall(debugFirstSwipe);
	}
	
	void Update ()
    {
        switch (currentState)
        {
            case SessionState.WaitingForSwipe:
                break;

            case SessionState.WaitingForBallRolling:
                break;

            case SessionState.WaitingForReset:
                break;

            default:
                Debug.LogError("Add logic for the state: " + currentState);
                break;
        }
	}

    //invoke events
    public void PinsDidFinishFalling(int numberOfPinsFallen)
    {
        lastScore = numberOfPinsFallen;

        ChangeState(SessionState.WaitingForReset);

        SessionResult result = SessionResult.Fail;

        if (numberOfPinsFallen > 0)
            result = SessionResult.Success;

        //if (didFinishSessionEvent != null)
        //    didFinishSessionEvent.Invoke(result, numberOfPinsFallen);
    }

    public void BallFellOverLane()
    {
        // if didn't hit any of the pins, change state, otherwise wait for pins to finish going to sleep
        if (!PinManager.instance.didStartWaitingForPinsToSleep)
        {
            ChangeState(SessionState.WaitingForReset);

            //if (didFinishSessionEvent != null)
            //    didFinishSessionEvent.Invoke(SessionResult.Fail, numberOfPinsFallen);
        }
    }

    //if you need any specific logic for session changes, add here:
    public void ChangeState(SessionState state)
    {
        Debug.Log("Changing State: " + state);
        currentState = state;

        if(currentState == SessionState.WaitingForReset)
            if (autoResetTime > 0)
                Invoke("ResetLane", autoResetTime);

        if (onSessionChangeEvent != null)
            onSessionChangeEvent.Invoke(currentState);
    }

    //called from an external source
    public void RollTheBall(Vector2 withViewportVelocity)
    {
        Vector3 physicalVelocity = withViewportVelocity * swipeVelocityMultiplier;

        BallStateManager.instance.RollTheBall(physicalVelocity);
        ChangeState(SessionState.WaitingForBallRolling);
    }

    //called from an external source (or automatically if autoResetTime > 0)
    public void ResetLane()
    {
        PinManager.instance.ResetPins();
        BallStateManager.instance.ResetBall();
        ChangeState(SessionState.WaitingForSwipe);
    }
}

public enum SessionState
{
    WaitingForSwipe,
    WaitingForBallRolling,
    WaitingForReset
}

public enum SessionResult
{
    Fail = 0,
    Success = 1
}

[System.Serializable]
public class DidFinishSessionEvent : UnityEvent<SessionResult, int> { }
[System.Serializable]
public class OnSessionChangeEvent : UnityEvent<SessionState> { }