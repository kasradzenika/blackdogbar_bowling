﻿using UnityEngine;
using UnityEngine.Events;

public class PinManager : MonoBehaviour
{
    public DoneCountingScore doneCountingScore;

    private GameObject[] pins;
    private Rigidbody[] pinBodies;
    private Vector3[] initialPositions;
    private int asleepBodiesCounter = 0;

    [HideInInspector]
    public bool didStartWaitingForPinsToSleep = false;

    public static PinManager instance;

    void Awake ()
    {
        instance = this;
        
        //initialize pins
        pins = GameObject.FindGameObjectsWithTag("pin");
        pinBodies = new Rigidbody[pins.Length];
        initialPositions = new Vector3[pins.Length];

        for (int i = 0; i < pins.Length; i++)
        {
            pinBodies[i] = pins[i].GetComponent<Rigidbody>();
            initialPositions[i] = pinBodies[i].position;
        }

        ResetPins();
    }

	void Update ()
    {
        asleepBodiesCounter = 0;

        foreach (Rigidbody pin in pinBodies)
        {
            if (pin.IsSleeping())
            {
                asleepBodiesCounter++;
            }
        }

        //first pin was awoken, start counting!
        if (!didStartWaitingForPinsToSleep && asleepBodiesCounter < pinBodies.Length)
        {
            Debug.Log("Waiting for all pins to fall");
            //waiting for all pins to go back to sleep
            didStartWaitingForPinsToSleep = true;
        }
        //was counting but all bodies went to sleep, now score
        else if (didStartWaitingForPinsToSleep && asleepBodiesCounter == pinBodies.Length)
        {
            didStartWaitingForPinsToSleep = false;

            //count score
            int score = 0;
            foreach (Rigidbody pin in pinBodies)
            {
                //if pin is not standing approximately vertical than score
                if (Vector3.Dot(pin.transform.up, Vector3.up) < 0.95f)
                    score++;
            }

            DoneCountingScore(score);
        }
	}

    void DoneCountingScore(int score)
    {
        Debug.Log("Scored: " + score);

        if(doneCountingScore != null)
            doneCountingScore.Invoke(score);
    }

    public void ResetPins()
    {
        for (int i = 0; i < pinBodies.Length; i++)
        {
            pinBodies[i].transform.position = initialPositions[i];
            pinBodies[i].transform.rotation = Quaternion.identity;
            pinBodies[i].velocity = Vector3.zero;
            pinBodies[i].Sleep();
        }

        didStartWaitingForPinsToSleep = false;
    }
}

[System.Serializable]
public class DoneCountingScore : UnityEvent<int> { }