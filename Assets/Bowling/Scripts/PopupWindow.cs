﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PopupWindow : MonoBehaviour
{
    CanvasGroup group;

    public string Text;
    public string Header;
    public Text TextHeader;
    public Text TextBody;
    void Awake()
    {
        group = GetComponent<CanvasGroup>();
    }

    public void ToggleOff()
    {
        group.interactable = false;
        group.blocksRaycasts = false;
        group.alpha = 0f;
    }

    public void TogggleOn()
    {
        group.interactable = true;
        group.blocksRaycasts = true;
        group.alpha = 1f;
        TextBody.text = Text;
        TextHeader.text = Header;
    }

    public void TogggleOn(string header, string body)
    {
        group.interactable = true;
        group.blocksRaycasts = true;
        group.alpha = 1f;
        TextBody.text = body;
        TextHeader.text = header;
    }
}
