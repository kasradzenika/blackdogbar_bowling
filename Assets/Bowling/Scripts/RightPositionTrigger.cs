﻿using UnityEngine;
using UnityEngine.Events;

public class RightPositionTrigger : MonoBehaviour
{
    public bool IsInArea;
    public UnityEvent InAreaOn;
    public UnityEvent InAreaOff;

    public UnityEvent InAreaChanged;


    void OnTriggerEnter(Collider other)
    {
        if (other.tag == "camera")
        {
            IsInArea = true;
            InAreaOn.Invoke();
        }
    }

    public void OnTriggerExit(Collider other)
    {
        if (other.tag == "camera")
        {
            IsInArea = false;
            InAreaOff.Invoke();
        }
    }
}
