﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ScoreManager : MonoBehaviour
{
    public Text ScoreLabel;
    public int Score;

    void Awake()
    {
        AddScore(0);
    }

    public void AddScore(int score)
    {
        this.Score += score;
        ScoreLabel.text = "Points: " + Score.ToString("D3");
    }
}
