﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class SetColorGroup : MonoBehaviour
{
    Image[] images;
    public void Awake()
    {
        images = GetComponentsInChildren<Image>();
    }

    public void SetColor(Color color)
    {
        if (images != null)
        {
            foreach (var item in images)
            {
                if (item != null)
                    item.color = color;
            }
        }
    }

}
