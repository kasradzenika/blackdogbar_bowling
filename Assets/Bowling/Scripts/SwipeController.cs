﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public class SwipeController : Selectable, IBeginDragHandler, IDragHandler, IEndDragHandler
{
    public Vector2 StartPost;
    public Vector2 Diff;

    public Vector2Event PlayerSwiped;

    public RightPositionTrigger InArea;

    public Text DebugLabel;

    public SessionState State;

    public SetColorGroup ColorGroup;

    public void Update()
    {
        if (InArea.IsInArea && State == SessionState.WaitingForSwipe)
        {
            ColorGroup.SetColor(Color.green);
        }
        else
        {
            ColorGroup.SetColor(Color.gray);
        }
    }

    public void OnDrag(PointerEventData eventData)
    {
        Diff = eventData.position - StartPost;
        DebugLabel.text = String.Format("X: {0}; Y:{1}; {2}", Diff.x, Diff.y, InArea.IsInArea);
    }

    public void OnBeginDrag(PointerEventData eventData)
    {
        StartPost = eventData.position;
    }

    public void OnEndDrag(PointerEventData eventData)
    {
        Diff = eventData.position - StartPost;
        var temp = new Vector2(Diff.x / 1020f, Diff.y / 1980f);
        DebugLabel.text = String.Format("X: {0}; Y:{1}; {2}", temp.x, temp.y, InArea.IsInArea);
        if (InArea.IsInArea && State == SessionState.WaitingForSwipe)
        {
            PlayerSwiped.Invoke(temp);
        }
    }

    public void StateChanged(SessionState state)
    {
        State = state;
        // switch (state)
        // {
        //     case SessionState.DisplayingResult:
        // 	LaneManager
        //     default:

        // }
    }
}
