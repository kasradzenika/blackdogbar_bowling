﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

[System.SerializableAttribute]
public class Vector2Event : UnityEvent<Vector2>
{

}
